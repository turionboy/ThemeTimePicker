package com.tt.themetimepicker;

import java.lang.reflect.Field;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TimePicker;

/**
 * 定义主题样式的时间选择器 <功能简述> <Br>
 * <功能详细描述> <Br>
 * 
 * @author Kyson www.hizhaohui.cn
 */
public class ThemeTimePicker extends TimePicker {

    public ThemeTimePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ThemeTimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ThemeTimePicker(Context context) {
        super(context);
    }

    public void setupTheme(int color, Drawable divider) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return;
        }
        setTimepickerTextColor(this, color, divider);
    }

    private void setTimepickerTextColor(TimePicker timePicker, int color,
            Drawable divider) {
        Resources system = Resources.getSystem();
        int hourNumberpickerId = system.getIdentifier("hour", "id", "android");
        int minuteNumberpickerId = system.getIdentifier("minute", "id",
                "android");
        int ampmNumberpickerId = system.getIdentifier("amPm", "id", "android");

        NumberPicker hourNumberpicker = (NumberPicker) timePicker
                .findViewById(hourNumberpickerId);
        NumberPicker minuteNumberpicker = (NumberPicker) timePicker
                .findViewById(minuteNumberpickerId);
        NumberPicker ampmNumberpicker = (NumberPicker) timePicker
                .findViewById(ampmNumberpickerId);

        setNumberpickerTextColor(hourNumberpicker, color, divider);
        setNumberpickerTextColor(minuteNumberpicker, color, divider);
        setNumberpickerTextColor(ampmNumberpicker, color, divider);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setNumberpickerTextColor(NumberPicker numberPicker, int color,
            Drawable divider) {
        if (numberPicker == null || color == 0) {
            return;
        }
        final int count = numberPicker.getChildCount();

        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);

            try {
                Field selectionDividerField = numberPicker.getClass()
                        .getDeclaredField("mSelectionDivider");
                selectionDividerField.setAccessible(true);
                selectionDividerField.set(numberPicker, divider);

                Field wheelpaintField = numberPicker.getClass()
                        .getDeclaredField("mSelectorWheelPaint");
                wheelpaintField.setAccessible(true);
                ((Paint) wheelpaintField.get(numberPicker)).setColor(color);
                if (child instanceof EditText) {
                    ((EditText) child).setTextColor(color);
                }
                numberPicker.invalidate();
            } catch (NoSuchFieldException e) {

            } catch (IllegalAccessException e) {

            } catch (IllegalArgumentException e) {

            }
        }
    }

}
