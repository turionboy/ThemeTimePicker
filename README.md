#TT日程管理V2.0开发系列3——自定义的时间选择器

- [TT日程管理V2.0开发系列1](http://www.hizhaohui.cn/archives/550)
- [TT日程管理V2.0开发系列2](http://www.hizhaohui.cn/archives/555)

###[项目开源地址](http://git.oschina.net/cocobaby/ThemeTimePicker)

###[TT日程管理应用下载](http://openbox.mobilem.360.cn/index/d/sid/2423472)

解决了时间选择器控件颜色，文字等跟随系统的问题，可以自定义颜色。

##项目演示：
![项目演示][1]

##使用说明：
基本与原生的时间选择器相同
```java
        mTimePicker = (ThemeTimePicker) this.findViewById(R.id.timepicker);
        mTimePicker.setIs24HourView(true);
        Calendar calendar = Calendar.getInstance();
        mTimePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
        mTimePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
        mTimePicker.setupTheme(
                getResources().getColor(R.color.material_green),
                getResources().getDrawable(
                        R.drawable.tl_picker_divider_green_shape));
```
###[更多使用方法](http://developer.android.com/reference/android/widget/TimePicker.html)

##更多

- [我的个人博客](http://www.hizhaohui.cn)

- [我的开源项目](http://git.oschina.net/cocobaby)

- [我的新浪微博](http://weibo.com/1980495343/profile?rightmod=1&wvr=6&mod=personinfo)

##License

Copyright (c) 2014 Kyson

Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)


  
 [1]: http://kkwordpress-wordpress.stor.sinaapp.com/uploads/2014/12/timepicker_showcase.gif